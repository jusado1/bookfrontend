import { HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bookfrontend';

  constructor(private router: Router) {
  }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('token_access');
  }

  logOut(): any {
    expireSession();
    this.router.navigateByUrl('');
  }

  getUsername(): string {
    const userStr = localStorage.getItem('user');
    if (userStr) {
      const user = JSON.parse(userStr);
      return user.username;
    }
    return 'Profile';
  }
}

export const expireSession = (doAlert = false) => {
  if (localStorage.getItem('token_access') == null) {
    return;
  }

  localStorage.removeItem('token_access');
  localStorage.removeItem('token_refresh');
  localStorage.removeItem('user');

  if (doAlert) {
    alert('Your sessions has been expired, please back log');
  }
};

export const getHeaders = (): HttpHeaders => {
  const token = localStorage.getItem('token_access');
  if (token) {
    return new HttpHeaders({ Authorization: 'Bearer ' + token });
  }
  return new HttpHeaders();
};
