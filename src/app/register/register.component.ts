import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    const body = form.value;
    console.log(form);

    this.http.post(environment.backend_url + '/api/users/', body)
      .subscribe(
        (value: any) => {
          console.log(value);
          this.http.post(environment.backend_url + '/api/token/', { username: value.username, password: body.password })
            .subscribe((value2: any) => {
              console.log(value2);
              localStorage.setItem('token_access', value2.access);
              localStorage.setItem('token_refresh', value2.refresh);
              localStorage.setItem('user', JSON.stringify(value2.user));
              this.router.navigateByUrl('');
            });
        },
        (error) => {
          if (error.error.username || error.error.password) {
            alert('Bad username or password');
          }

          if (error.error.email) {
            alert(error.error.email);
          }
        }
      );
  }

}
