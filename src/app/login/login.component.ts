import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    const body = form.value;
    console.log(body);

    this.http.post(environment.backend_url + '/api/token/', body)
      .subscribe(
        (value: any) => {
          console.log(value);
          localStorage.setItem('token_access', value.access);
          localStorage.setItem('token_refresh', value.refresh);
          localStorage.setItem('user', JSON.stringify(value.user));
          this.router.navigateByUrl('');
        },
        (error) => {
          if (error.error.username || error.error.password || error.error.detail) {
            alert('Bad username or password');
          }
        }
      );
  }
}
