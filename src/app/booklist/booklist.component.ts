import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { expireSession, getHeaders } from '../app.component';

@Component({
  selector: 'app-booklist',
  templateUrl: './booklist.component.html',
  styleUrls: ['./booklist.component.scss']
})
export class BooklistComponent implements OnInit {

  books: any[];
  selected: any;
  edit: boolean;

  constructor(private http: HttpClient) {
    this.books = [];
    this.selected = 0;
    this.edit = false;
  }

  ngOnInit(): void {
    this.http.get(environment.backend_url + '/api/books/')
      .subscribe((data: any) => {
        console.log(data);
        this.books = data;
        this.selected = this.books[0];
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  deleteBook(id: number): void {
    if (!confirm('Are you sure want to delete this book?')) {
      return;
    }
    const reqHeaders = getHeaders();

    this.http.delete(environment.backend_url + '/api/books/' + id, { headers: reqHeaders })
      .subscribe(value => {
        console.log(value);
        this.loadBook(id, value);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  loadBook(id: number, data?: any): void {
    console.log(id, data);
    const findandupdate = () => {
      for (let i = 0; i < this.books.length; i++) {
        if (this.books[i].id === id) {
          data == null ? this.books.splice(i, 1) : this.books[i] = data;
          return;
        }
      }
      this.books.unshift(data);
    };

    if (data === undefined) {
      const reqHeaders = getHeaders();
      this.http.get(environment.backend_url + '/api/books/' + id, { headers: reqHeaders })
        .subscribe((data2: any) => {
          data = data2;
          console.log(data);
          findandupdate();
        },
          error => {
            if (error.status === 401) {
              expireSession(true);
            }
          }
        );
    } else {
      findandupdate();
    }
  }

  isAdmin(): boolean {
    const userStr = localStorage.getItem('user');
    if (userStr) {
      const user = JSON.parse(userStr);
      return user.is_superuser;
    }
    return false;
  }
}
