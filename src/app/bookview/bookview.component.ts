import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { expireSession, getHeaders } from '../app.component';

@Component({
  selector: 'app-bookview',
  templateUrl: './bookview.component.html',
  styleUrls: ['./bookview.component.scss']
})
export class BookviewComponent implements OnInit, OnChanges {

  @Input() book: any;
  @Input() edit: boolean;

  @Output() newItemEvent = new EventEmitter<any>();

  @ViewChild('heroForm') heroForm!: NgForm;

  summaries: any[];

  constructor(private http: HttpClient) {
    this.summaries = [];
    this.edit = false;
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.book);
    if (this.book) {
      this.loadSummaries();
    }
    this.heroForm?.resetForm();
  }

  isOwnerOrAdmin(author: string): boolean {
    const userStr = localStorage.getItem('user');
    if (userStr) {
      const user = JSON.parse(userStr);
      return user.username === author || user.is_superuser;
    }
    return false;
  }

  loadSummaries(): void {
    const reqHeaders = getHeaders();
    this.http.get(environment.backend_url + '/api/books/' + this.book.id + '/summaries/', { headers: reqHeaders })
      .subscribe((data: any) => {
        this.summaries = data; console.log(data);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  loadSummary(id: number, data?: any): void {
    const findandupdate = () => {
      for (let i = 0; i < this.summaries.length; i++) {
        if (this.summaries[i].id === id) {
          data == null ? this.summaries.splice(i, 1) : this.summaries[i] = data;
          break;
        }
      }
    };

    if (data === undefined) {
      const reqHeaders = getHeaders();
      this.http.get(environment.backend_url + '/api/summaries/' + id, { headers: reqHeaders })
        .subscribe((data2: any) => {
          data = data2;
          console.log(data);
          findandupdate();
        },
          error => {
            if (error.status === 401) {
              expireSession(true);
            }
          }
        );
    } else {
      findandupdate();
    }
  }

  upvote(id: number, status: number): void {
    this.vote(id, status, 1);
  }

  downvote(id: number, status: number): void {
    this.vote(id, status, -1);
  }

  vote(id: number, status: number, newRating: number): void {
    if (!this.isLoggedIn()) {
      alert('Please log in before voting');
      return;
    }
    if (status === newRating) {
      newRating = 0;
    }
    console.log(status, newRating);

    const body = {
      summary: id,
      rating: newRating
    };
    const reqHeaders = getHeaders();

    this.http.post(environment.backend_url + '/api/votes/', body, { headers: reqHeaders })
      .subscribe(value => {
        console.log(value);
        // this.loadSummary(id);
        this.loadSummaries();
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  createSummary(form: NgForm): void {
    const body = form.value;
    body.book = this.book.id;

    console.log(body);
    const reqHeaders = getHeaders();

    this.http.post(environment.backend_url + '/api/summaries/', body, { headers: reqHeaders })
      .subscribe((value: any) => {
        console.log(value);
        this.summaries.unshift(value);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );

    form.resetForm();
  }

  updateSummary(id: number, newText: string): void {
    if (!confirm('Are you sure want to edit this summary?')) {
      return;
    }

    if (!newText) {
      return;
    }
    console.dir(newText);
    const reqHeaders = getHeaders();

    this.http.patch(environment.backend_url + '/api/summaries/' + id + '/', { text: newText }, { headers: reqHeaders })
      .subscribe(value => {
        console.log(value);
        this.loadSummary(id, value);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  deleteSummary(id: number): void {
    if (!confirm('Are you sure want to delete this summary?')) {
      return;
    }
    const reqHeaders = getHeaders();

    this.http.delete(environment.backend_url + '/api/summaries/' + id, { headers: reqHeaders })
      .subscribe(value => {
        console.log(value);
        this.loadSummary(id, value);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  createBook(form: NgForm): void {
    if (!confirm('Are you sure want to create this book?')) {
      return;
    }
    const body = form.value;

    console.log(body);
    const reqHeaders = getHeaders();

    this.http.post(environment.backend_url + '/api/books/', body, { headers: reqHeaders })
      .subscribe((value: any) => {
        console.log(value);
        this.newItemEvent.emit(value);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );

    form.resetForm();
  }

  updateBook(form: NgForm): void {
    if (!confirm('Are you sure want to edit this book?')) {
      return;
    }
    const id = this.book.id;
    const body = form.value;
    console.log(body);
    const reqHeaders = getHeaders();

    this.http.patch(environment.backend_url + '/api/books/' + id + '/', body, { headers: reqHeaders })
      .subscribe((value: any) => {
        console.log(value);
        this.book.title = value.title;
        this.newItemEvent.emit(value);
      },
        error => {
          if (error.status === 401) {
            expireSession(true);
          }
        }
      );
  }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('token_access');
  }
}
