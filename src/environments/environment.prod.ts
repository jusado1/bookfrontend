export const environment = {
  production: true,
  backend_url: 'https://book-summary-site.herokuapp.com'
};
